var __entityMap = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': '&quot;',
	"'": '&#39;',
	"/": '&#x2F;'
};

String.prototype.escapeHTML = function() {
	return String(this).replace(/[&<>"'\/]/g, function(s) {
		return __entityMap[s];
	});
}

String.prototype.explode = function(separator, limit) {
	var arr = this.split(separator);
	if (limit) arr.push(arr.splice(limit - 1).join(separator));
	return arr;
}

var gui = require("nw.gui");
var fs = require("fs");
var path = require("path");
var http = require("http");
var https = require("follow-redirects").https;
var execFile = require("child_process").execFile;
var execSync = require("child_process").execSync;
var exec = require("child_process").exec;
var win = gui.Window.get();

var nrs = null;
var minter = null;
var dirSeparator = "/";
var isStarted = false;
var isTestNet = false;
var serverPort = "";
var nxtDirectory = "";
var nxtVersion = "";
var systemClose = false;
var platform = "";
var javaLocations = ["java"];
var selectedJavaLocation;
var currentLocationTest = 0;
var startCommand = "nxt.jar:lib/*:conf";
var netSwitcher = null;
var is64bit = true;
var manuallyCheckingForUpdates = false;
var nxtInitializationError = false;
var serverOutput = [];
var minterOutput = [];
var doNotQuit = false;
var preferences = {};
var minterPreferences = {};
var tray = null;
var updateInterval = null;
var defaultPrefs = {
	"minimizeToTray": 0,
	"checkForUpdates": 1
};
var minimizeCallback = null;
var showQuitAlert = false;
var callback = null;
var startStopMintingMenuItem;
var javaCommand;
var mintCommand;

clearCache();
loadPreferences();
checkPlatform();
prepareWindow();
createMenuBar();
addTrayBehavior();

function clearCache(complete) {
	var cacheDirectory = gui.App.dataPath + (process.platform == "win32" ? "\\" : "/") + "Cache";

	if (fs.existsSync(cacheDirectory)) {
		var rmdir = require("rimraf");

		rmdir(cacheDirectory, function(error) {
			if (complete) {
				complete();
			}
		});
	} else if (complete) {
		complete();
	}
}

function loadPreferences() {
	var storedPrefs = localStorage.getItem("preferences");

	if (!storedPrefs) {
		storedPrefs = defaultPrefs;
	} else {
		try {
			storedPrefs = JSON.parse(storedPrefs);
		} catch (e) {
			storedPrefs = defaultPrefs;
		}
	}

	preferences = storedPrefs;
}

function savePreferences() {
	if (platform == "win") {
		var minimizeToTray = (document.getElementById("minimize_to_tray").checked ? 1 : 0);
	} else {
		var minimizeToTray = 0;
	}

	var checkForUpdates = (document.getElementById("check_for_updates").checked ? (document.getElementById("check_for_beta_updates").checked ? 2 : 1) : 0);

	preferences = {
		"minimizeToTray": minimizeToTray,
		"checkForUpdates": checkForUpdates
	};

	localStorage.setItem("preferences", JSON.stringify(preferences));

	addUpdateBehavior();
	addTrayBehavior();
}

function saveMintingPreferences() {
	addToConfig({
		"nxt.mint.serverAddress": $("#mintServerAddress").val(),
		"nxt.mint.currencyCode": $("#mintCurrencyCode").val(),
		"nxt.mint.secretPhrase": $("#mintSecretPhrase").val(),
		"nxt.mint.unitsPerMint": $("#mintUnitsPerMint").val(),
		"nxt.mint.threadPoolSize": $("#mintThreadPoolSize").val(),
		"nxt.mint.initialNonce": $("#mintInitialNonce").val(),
		"nxt.mint.isSubmitted": $("#mintIsSubmitted").is(":checked") ? "true" : "false",
		"nxt.mint.useHttps": $("#mintUseHttps").is(":checked") ? "true" : "false"
	});
}

function showPreferencesWindow() {
	if (showQuitAlert) {
		return;
	}

	hideAlerts();

	bootbox.dialog({
		message: (platform == "win" ? "<div class='checkbox'><input type='checkbox' name='minimize_to_tray' id='minimize_to_tray' value='1' " + (preferences.minimizeToTray ? " checked='checked'" : "") + " style='margin-left:0' /> <label for='minimize_to_tray'>Minimize to tray</label></div>" : "") +
			"<div class='checkbox'><input type='checkbox' name='check_for_updates' id='check_for_updates' value='1' " + (preferences.checkForUpdates != 0 ? " checked='checked'" : "") + " style='margin-left:0' /> <label for='check_for_updates'>Automatically check for updates</label></div>" +
			"<div class='checkbox'><input type='checkbox' name='check_for_beta_updates' id='check_for_beta_updates' value='1' " + (preferences.checkForUpdates == 2 ? " checked='checked'" : "") + " style='margin-left:0' /> <label for='check_for_beta_updates'>Update to beta versions when available</label></div>",
		title: "Preferences",
		buttons: {
			save: {
				label: "OK",
				className: "btn-primary",
				callback: function() {
					savePreferences();
				}
			}
		}
	});
}

function showMintingPreferencesWindow() {
	if (showQuitAlert) {
		return;
	}

	hideAlerts();

	var mintingPreferences = getConfig(["nxt.mint.serverAddress",
		"nxt.mint.currencyCode",
		"nxt.mint.secretPhrase",
		"nxt.mint.unitsPerMint",
		"nxt.mint.threadPoolSize",
		"nxt.mint.initialNonce",
		"nxt.mint.isSubmitted",
		"nxt.mint.useHttps"
	]);

	if (!mintingPreferences["nxt.mint.serverAddress"]) {
		mintingPreferences["nxt.mint.serverAddress"] = "localhost";
	}
	if (!mintingPreferences["nxt.mint.currencyCode"]) {
		mintingPreferences["nxt.mint.currencyCode"] = "";
	}
	if (!mintingPreferences["nxt.mint.secretPhrase"]) {
		mintingPreferences["nxt.mint.secretPhrase"] = "";
	}
	if (!mintingPreferences["nxt.mint.unitsPerMint"]) {
		mintingPreferences["nxt.mint.unitsPerMint"] = "1";
	}
	if (!mintingPreferences["nxt.mint.threadPoolSize"]) {
		mintingPreferences["nxt.mint.threadPoolSize"] = "1";
	}
	if (!mintingPreferences["nxt.mint.initialNonce"]) {
		mintingPreferences["nxt.mint.initialNonce"] = "0";
	}
	if (!mintingPreferences["nxt.mint.isSubmitted"]) {
		mintingPreferences["nxt.mint.isSubmitted"] = "true";
	}
	if (!mintingPreferences["nxt.mint.useHttps"]) {
		mintingPreferences["nxt.mint.useHttps"] = "false";
	}

	bootbox.dialog({
		message: "<form class='form-horizontal'>" +
			"<div class='form-group'><label for='mintServerAddress' class='col-sm-3 control-label'>Server Address</label><div class='col-sm-9'><input type='text' class='form-control' id='mintServerAddress' name='serverAddress' value='" + String(mintingPreferences["nxt.mint.serverAddress"]).escapeHTML() + "' /></div></div>" +
			"<div class='form-group'><label for='mintCurrencyCode' class='col-sm-3 control-label'>Currency Code</label><div class='col-sm-9'><input type='text' class='form-control' id='mintCurrencyCode' name='currencyCode' value='" + String(mintingPreferences["nxt.mint.currencyCode"]).escapeHTML() + "' /></div></div>" +
			"<div class='form-group'><label for='mintSecretPhrase' class='col-sm-3 control-label'>Secret Phrase</label><div class='col-sm-9'><input type='password' class='form-control' id='mintSecretPhrase' name='secretPhrase' value='" + String(mintingPreferences["nxt.mint.secretPhrase"]).escapeHTML() + "' /></div></div>" +
			"<div class='form-group'><label for='mintUnitsPerMint' class='col-sm-3 control-label'>Units Per Mint</label><div class='col-sm-9'><input type='number' class='form-control' id='mintUnitsPerMint' name='unitsPerMint' value='" + String(mintingPreferences["nxt.mint.unitsPerMint"]).escapeHTML() + "' /></div></div>" +
			"<div class='form-group'><label for='mintThreadPoolSize' class='col-sm-3 control-label'>Thread Pool Size</label><div class='col-sm-9'><input type='number' class='form-control' id='mintThreadPoolSize' name='threadPoolSize' value='" + String(mintingPreferences["nxt.mint.threadPoolSize"]).escapeHTML() + "' /></div></div>" +
			"<div class='form-group'><label for='mintInitialNonce' class='col-sm-3 control-label'>Initial Nonce</label><div class='col-sm-9'><input type='number' class='form-control' id='mintInitialNonce' name='initialNonce' value='" + String(mintingPreferences["nxt.mint.initialNonce"]).escapeHTML() + "' /></div></div>" +
			"<div class='form-group'><label for='mintIsSubmitted' class='col-sm-3 control-label'>Is Submitted</label><div class='col-sm-9' style='padding-top:5px'><input type='checkbox' id='mintIsSubmitted' name='isSubmitted' value='1' " + (mintingPreferences["nxt.mint.isSubmitted"] == "true" ? " checked='checked'" : "") + " /></div></div>" +
			"<div class='form-group'><label for='mintUseHttps' class='col-sm-3 control-label'>Use Https</label><div class='col-sm-9' style='padding-top:5px;'><input type='checkbox' id='mintUseHttps' name='useHttps' value='1' " + (mintingPreferences["nxt.mint.useHttps"] == "true" ? " checked='checked'" : "") + " /></div></div>" +
			"</form>",
		title: "Minting Preferences",
		buttons: {
			save: {
				label: "Save",
				className: "btn-primary",
				callback: function() {
					saveMintingPreferences();
				}
			}
		}
	});
}

function checkPlatform() {
	if (process.platform == "darwin") {
		platform = "mac";
		javaLocations.push("/Library/Internet Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java");
		//usr/libexec/java_home -v 1.7
	} else if (process.platform == "win32") {
		platform = "win";
		javaLocations.push("C:\\Program Files\\Java\\jre7\\bin\\java.exe");
		javaLocations.push("C:\\Program Files (x86)\\Java\\jre7\\bin\\java.exe");
		javaLocations.push("C:\\Program Files\\Java\\jre8\\bin\\java.exe");
		javaLocations.push("C:\\Program Files (x86)\\Java\\jre8\\bin\\java.exe");
		startCommand = "nxt.jar;lib\\*;conf";
		dirSeparator = "\\";

		if (process.arch === "x64" || process.env.hasOwnProperty("PROCESSOR_ARCHITEW6432")) {
			is64bit = true;
		} else {
			is64bit = false;
		}
	} else {
		platform = "linux";
		javaLocations.push("/usr/bin/java");
	}
}

function prepareWindow() {
	win.on("close", function() {
		systemClose = true;
		doNotQuit = true;

		killNrs(function() {
			clearCache(function() {
				win.hide();
				gui.App.quit();
			});
		})
	});

	win.on("new-win-policy", function(frame, url, policy) {
		policy.ignore();
		require("nw.gui").Shell.openExternal(url);
	});
}

function showContextMenu(e) {
	try {
		e.preventDefault();
		var gui = require("nw.gui");
		var menu = new gui.Menu();

		var isInputField = e.toElement && (e.toElement.nodeName == "INPUT" || e.toElement.nodeName == "TEXTAREA");

		var selectedText = document.getElementById("nrs").contentWindow.getSelection().toString();

		var cut = new gui.MenuItem({
			label: "Cut",
			enabled: (isInputField && selectedText ? true : false),
			click: function() {
				try {
					document.getElementById("nrs").contentWindow.document.execCommand("cut");
				} catch (e) {}
			}
		});

		var copy = new gui.MenuItem({
			label: "Copy",
			enabled: (selectedText ? true : false),
			click: function() {
				try {
					document.getElementById("nrs").contentWindow.document.execCommand("copy");
				} catch (e) {}
			}
		});

		var paste = new gui.MenuItem({
			label: "Paste",
			enabled: isInputField,
			click: function() {
				try {
					document.getElementById("nrs").contentWindow.document.execCommand("paste");
				} catch (e) {}
			}
		});

		menu.append(cut);
		menu.append(copy);
		menu.append(paste);

		menu.popup(e.x, e.y);
	} catch (e) {}

	return false;
}

function createMenuBar() {
	var menubar = new gui.Menu({
		type: "menubar"
	});

	var toolsMenu = new gui.Menu();

	menubar.append(new gui.MenuItem({
		label: "Tools",
		submenu: toolsMenu
	}));

	toolsMenu.append(new gui.MenuItem({
		label: "Check For Updates",
		click: function() {
			manuallyCheckingForUpdates = true;
			checkForUpdates();
		}
	}));

	toolsMenu.append(new gui.MenuItem({
		label: "Preferences",
		click: function() {
			showPreferencesWindow();
		}
	}));

	toolsMenu.append(new gui.MenuItem({
		type: "separator"
	}));

	netSwitcher = new gui.MenuItem({
		label: "Switch Net",
		//enabled: false,
		click: function() {
			switchNet();
		}
	});

	toolsMenu.append(netSwitcher);

	toolsMenu.append(new gui.MenuItem({
		label: "Redownload Blockchain",
		click: function() {
			redownloadBlockchain();
		}
	}));

	toolsMenu.append(new gui.MenuItem({
		label: "View Server Log",
		click: function() {
			viewServerLog();
		}
	}));

	var mintingMenu = new gui.Menu();

	menubar.append(new gui.MenuItem({
		label: "Minting",
		submenu: mintingMenu
	}));

	mintingMenu.append(new gui.MenuItem({
		label: "Preferences",
		click: function() {
			showMintingPreferencesWindow();
		}
	}));

	startStopMintingMenuItem = new gui.MenuItem({
		label: "Start Minting",
		click: function() {
			startStopMinting();
		}
	});

	mintingMenu.append(startStopMintingMenuItem);

	mintingMenu.append(new gui.MenuItem({
		label: "View Minter Log",
		click: function() {
			viewMinterLog();
		}
	}))

	var helpMenu = new gui.Menu();

	helpMenu.append(new gui.MenuItem({
		label: "Nxt.org",
		click: function() {
			require("nw.gui").Shell.openExternal("http://nxt.org/");
		}
	}));

	helpMenu.append(new gui.MenuItem({
		label: "Support Forum",
		click: function() {
			require("nw.gui").Shell.openExternal("https://nxtforum.org/");
		}
	}));

	helpMenu.append(new gui.MenuItem({
		label: "Wiki",
		click: function() {
			require("nw.gui").Shell.openExternal("http://www.thenxtwiki.org/");
		}
	}));

	helpMenu.append(new gui.MenuItem({
		label: "IRC Channel",
		click: function() {
			require("nw.gui").Shell.openExternal("https://kiwiirc.com/client/irc.freenode.org/#nxtalk");
		}
	}));

	menubar.append(new gui.MenuItem({
		label: "Help",
		submenu: helpMenu
	}));

	win.menu = menubar;
}

function addTrayBehavior() {
	if (platform != "win") {
		return;
	}

	if (preferences.minimizeToTray) {
		if (!minimizeCallback) {
			minimizeCallback = function() {
				this.hide();

				tray = new gui.Tray({
					title: "Nxt Wallet",
					tooltip: "Nxt Wallet",
					icon: "img/logo.png"
				});

				tray.on("click", function() {
					win.show();
					win.focus();
					this.remove();
					tray = null;
				});
			}
		}

		win.on("minimize", minimizeCallback);
	} else if (minimizeCallback) {
		win.removeListener("minimize", minimizeCallback);
		minimizeCallback = null;
	}
}

function findNxtDirectory(callback) {
	var cwd = path.dirname(process.execPath);

	var sourceDirectory = cwd + dirSeparator + "nxt" + dirSeparator;

	var doInitialCopy = false;

	if (platform == "win") {
		nxtDirectory = gui.App.dataPath + dirSeparator + "nxt" + dirSeparator;

		if (!fs.existsSync(sourceDirectory)) {
			alert("Could not find the Nxt directory. Make sure it is in the same folder as this app, and named 'nxt'.");
			win.close();
		} else {
			if (!fs.existsSync(nxtDirectory)) {
				doInitialCopy = true;
			} else {
				var testLocation = "classes" + dirSeparator + "nxt" + dirSeparator + "Nxt.class";

				if (!fs.existsSync(sourceDirectory + testLocation)) {
					if (fs.existsSync(sourceDirectory + "nxt.jar")) {
						testLocation = "nxt.jar";
					} else {
						testLocation = "src" + dirSeparator + "java" + dirSeparator + "nxt" + dirSeparator + "Nxt.java";
					}
				}

				var statSource = fs.statSync(sourceDirectory + testLocation);

				if (!fs.existsSync(nxtDirectory + testLocation)) {
					var statDestination = null;
				} else {
					var statDestination = fs.statSync(nxtDirectory + testLocation);
				}

				if (!statDestination || statDestination.mtime.getTime() < statSource.mtime.getTime()) {
					doInitialCopy = true;
				}
			}

			if (doInitialCopy) {
				var ncp = require("ncp").ncp;

				ncp.limit = 16;

				ncp(sourceDirectory, nxtDirectory, function(err) {
					if (err) {
						alert("Could not install the Nxt directory.");
						win.close();
					} else {
						callback();
					}
				});
			}
		}
	} else {
		nxtDirectory = sourceDirectory;

		if (platform == "mac") {
			var pos = nxtDirectory.indexOf("Nxt Wallet.app");

			if (pos != -1) {
				nxtDirectory = nxtDirectory.substring(0, pos) + "nxt" + dirSeparator;
			}
		}
	}

	if (!doInitialCopy) {
		if (!fs.existsSync(nxtDirectory)) {
			alert("Could not find the Nxt directory. Make sure it is in the same folder as this app, and named 'nxt'.");
			win.close();
		} else {
			callback();
		}
	}
}

function relaunchApplication() {
	require("nw.gui").Window.get().reload();
}

function startServer() {
	checkJavaLocation(javaLocations[currentLocationTest]);
}

//execFile is asynchronous...
function checkJavaLocation(location) {
	var found = false;

	if (location == "java" || fs.existsSync(location)) {
		try {
			var error = false;

			var child = execFile(location, ["-version"]);

			child.stderr.on("data", function(data) {
				var java_version = data.match(/java version "([0-9\.]+)/i);

				if (java_version && java_version[1] && versionCompare(java_version[1], "1.7.0") != -1) {
					found = true;
					startServerProcess(location);
				}
			});

			child.on("error", function(e) {
				error = true;
				checkNextJavaLocation();
			});

			child.on("exit", function() {
				//wait 1 second before going to the next one...
				setTimeout(function() {
					if (!found && !error) {
						checkNextJavaLocation();
					}
				}, 1000);
			});
		} catch (err) {
			checkNextJavaLocation();
		}
	} else {
		checkNextJavaLocation();
	}
}

function checkNextJavaLocation() {
	currentLocationTest++;
	if (javaLocations[currentLocationTest]) {
		checkJavaLocation(javaLocations[currentLocationTest]);
	} else {
		showNoJavaInstalledWindow();
	}
}

function getUserHome() {
	return process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
}

function showNoJavaInstalledWindow() {
	if (showQuitAlert) {
		return;
	}

	document.getElementById("loading_indicator_container").style.display = "none";
	document.getElementById("server_output").innerHTML = "";

	bootbox.dialog({
		message: "<p>The correct java version was not found on your system. Click on the button below to start downloading." + (platform != "win" ? " Reopen the app after installation." : "") + "</p>" +
			"<div class='progress progress-striped active' style='margin-top:10px;margin-bottom:0;'>" +
			"<div id='java_download_progress' class='progress-bar' role='progressbar' aria-valuemin='0' aria-valuemax='100' style='width:0%'>" +
			"<span class='sr-only'>0% Complete</span>" +
			"</div>" +
			"</div>",
		title: "Java Not Found...",
		closeButton: false,
		buttons: {
			cancel: {
				label: "Cancel (Quit)",
				className: "btn-default",
				callback: function() {
					win.close();
				}
			},
			download: {
				label: "Download",
				className: "btn-primary",
				callback: function() {
					downloadJava();
					$(".bootbox button").attr("disabled", true);
					return false;
				}
			}
		}
	});
}

function downloadJava() {
	var url = require("url");

	var pageReq = http.get("http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html", function(res) {
		var body = "";

		res.on("data", function(chunk) {
			body += chunk;
		}).on("end", function() {
			var filename, regexFilename, extension;

			switch (platform) {
				case "win":
					if (is64bit) {
						filename = "windows-x64.exe";
						regexFilename = "windows\-x64\.exe";
					} else {
						filename = "windows-i586.exe";
						regexFilename = "windows\-i586\.exe";
					}
					extension = "exe";
					break;
				case "mac":
					filename = "macosx-x64.dmg";
					regexFilename = "macosx\-x64\.dmg";
					extension = "dmg";
					break;
				case "linux":
					filename = "linux-x64.rpm";
					regexFilename = "macosx\-x64\.dmg";
					extension = "rpm";
					break;
			}

			var regex = new RegExp("http:\/\/download\.oracle\.com\/otn\-pub\/java\/jre\/[a-z0-9\-\/]+" + regexFilename, "i");

			var downloadUrl = body.match(regex);

			if (downloadUrl && downloadUrl[0]) {
				downloadUrl = downloadUrl[0];
			} else {
				downloadUrl = "http://download.oracle.com/otn-pub/java/jdk/7u55-b13/jre-7u55-" + filename;
			}

			downloadUrl = downloadUrl.replace(/http:\/\//i, "https://");
			downloadUrl = downloadUrl.replace(/download\.oracle\.com/i, "edelivery.oracle.com");

			var downloadUrlParts = url.parse(downloadUrl);

			var options = {
				hostname: downloadUrlParts.hostname,
				path: downloadUrlParts.path,
				port: 443,
				method: "GET",
				rejectUnauthorized: false,
				headers: {
					"Referer": "http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
					"Accept-Language": "en-us",
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14",
					"Accept-Encoding": "gzip,deflate",
					"Cookie": "s_nr=" + new Date().getTime() + ";gpw_e24=http%3A%2F%2Fwww.oracle.com%2Ftechnetwork%2Fjava%2Fjavase%2Fdownloads%2Fjre7-downloads-1880261.html;s_cc=true;s_sq=%5B%5BB%5D%5D;oraclelicense=accept-securebackup-cookie",
					"Host": "edelivery.oracle.com"
				}
			};

			var fileReq = https.request(options, function(res) {
				if (res.statusCode == 302 && res.headers.location) {
					downloadUrlParts = url.parse(res.headers.location);

					options.hostname = downloadUrlParts.hostname;
					options.path = downloadUrlParts.path;
					options.headers["Host"] = downloadUrlParts.hostname;

					var downloadLocation = getUserHome() + dirSeparator + "nxt-java-install." + extension

					var downloadReq = https.request(options, function(res) {
						var len = parseInt(res.headers["content-length"], 10);
						var cur = 0;
						var total = len / 1048576; //bytes in 1 MB

						var out = fs.createWriteStream(downloadLocation);

						res.pipe(out);

						res.on("data", function(chunk) {
							cur += chunk.length;
							document.getElementById("java_download_progress").style.width = (100.0 * cur / len).toFixed(2) + "%";
						}).on("end", function() {
							if (platform == "win") {
								//do silent install
								hideAlerts();

								bootbox.dialog({
									message: "<p>Java is being installed, please wait...</p>",
									title: "Installing Java...",
									closeButton: false
								});

								var exec = require("child_process").exec;

								try {
									var child = exec(downloadLocation + " /s", function(error, stdout, stderr) {
										if (error != null) {
											showAlertAndQuit("Installation failed, please install manually. The setup filed is located at " + downloadLocation + ".");
										} else {
											//ok, it's installed
											child.kill();

											relaunchApplication();
										}
									});
								} catch (e) {
									showAlertAndQuit("Installation failed, please install manually. The setup filed is located at " + downloadLocation + ".");
								}
							} else if (platform == "mac") {
								gui.Shell.openItem(downloadLocation);
								setTimeout(function() {
									win.close();
								}, 2000);
							} else {
								//give notice to the user to install manually
								showAlertAndQuit("Java has been downloaded to " + downloadLocation + " - please install it and reopen this app after installation.");
							}
						}).on("error", function(e) {
							downloadJavaFailed();
						});
					}).on("error", function(e) {
						downloadJavaFailed();
					});

					downloadReq.end();
				} else {
					downloadJavaFailed();
				}
			}).on("error", function(e) {
				downloadJavaFailed();
			});

			fileReq.end();
		});
	}).on("error", function(e) {
		downloadJavaFailed();
	});

	pageReq.end();
}

function downloadJavaFailed() {
	showAlertAndQuit("Download failed, please <a href='http://www.java.com/en/download/manual.jsp'>click here</a> to download and install java manually.");
}

function startStopMinting() {
	if (minter) {
		if (minter.exitCode == null) {
			minter.kill();
		}
		killAlreadyRunningProcess("minter");
		minter = null;
		startStopMintingMenuItem.label = "Start Minting";
		startStopMintingMenuItem.enabled = false;
		startStopMintingMenuItem.enabled = true; //hack to make the label update
		return;
	}

	startStopMintingMenuItem.label = "Stop Minting";
	startStopMintingMenuItem.enabled = false;
	startStopMintingMenuItem.enabled = true;

	try {
		var startCommand;

		if (platform == "win") {
			startCommand = "classes;lib\\*;conf";
		} else {
			startCommand = "classes:lib/*:conf";
		}

		mintCommand = "\"" + selectedJavaLocation + "\" -cp " + startCommand + " nxt.mint.MintWorker";

		var pid = getAlreadyRunningProcess(mintCommand);

		if (pid) {
			killAlreadyRunningProcess("minter");
		}

		minter = execFile(selectedJavaLocation, ["-cp", startCommand, "nxt.mint.MintWorker"], {
			"cwd": nxtDirectory,
			"detached": true
		});

		minter.stdout.on("data", function(data) {
			logMinterOutput(data);
		});

		minter.stderr.on("data", function(data) {
			logMinterOutput(data);
		});
	} catch (err) {
		minter = null;
		startStopMintingMenuItem.label = "Start Minting";
		startStopMintingMenuItem.enabled = false;
		startStopMintingMenuItem.enabled = true;
		showAlert("Unknown minter error occurred.");
	}

}

function showAlreadyRunningProcessAlert() {
	hideAlerts();

	document.getElementById("loading_indicator_container").style.display = "none";
	document.getElementById("server_output_container").style.display = "none";

	bootbox.dialog({
		message: "<p>A NXT process is already running. If you have another NXT application running, please quit it before continuing. Otherwise, click 'Continue'.",
		title: "NXT Process Already Running",
		buttons: {
			cancel: {
				label: "Cancel",
				className: "btn-default",
				callback: function() {
					win.close();
				}
			},
			update: {
				label: "Continue",
				className: "btn-primary",
				callback: function() {
					killAlreadyRunningProcessAndRestart();
				}
			}
		}
	});
}

function killAlreadyRunningProcessAndRestart() {
	killAlreadyRunningProcess(null, true);
	relaunchApplication();
}

function killAlreadyRunningProcess(type, wait) {
	var pid;

	if (!type || type == "minter") {
		pid = getAlreadyRunningProcess(mintCommand);

		if (pid) {
			try {
				exec("taskkill /F /T /PID " + pid);

				if (wait) {
					while (getAlreadyRunningProcess(mintCommand)) {
						//wait..
					}
				}
			} catch (err) {}
		}
	}

	if (!type || type == "java") {
		pid = getAlreadyRunningProcess(javaCommand);

		if (pid) {
			try {
				exec("taskkill /F /T /PID " + pid);

				if (wait) {
					while (getAlreadyRunningProcess(javaCommand)) {
						//wait..
					}
				}
			} catch (err) {}
		}
	}
}

function startServerProcess(javaLocation) {
	serverPort = "7876";

	selectedJavaLocation = javaLocation;

	try {
		var startCommand;

		if (platform == "win") {
			startCommand = "classes;lib\\*;conf";
		} else {
			startCommand = "classes:lib/*:conf";
		}

		javaCommand = "\"" + javaLocation + "\" -Xmx1024M -cp " + startCommand + " nxt.Nxt";

		var pid = getAlreadyRunningProcess(javaCommand);

		if (pid) {
			showAlreadyRunningProcessAlert();
			return;
		}

		nrs = execFile(javaLocation, ["-Xmx1024M", "-cp", startCommand, "nxt.Nxt"], {
			"cwd": nxtDirectory,
			"detached": true
		});

		nrs.stdout.on("data", function(data) {
			logServerOutput(data);
			checkServerOutput(data);
		});

		nrs.stderr.on("data", function(data) {
			logServerOutput(data);
			checkServerOutput(data);

			if (!nxtInitializationError && data.match(/java\.lang\.ExceptionInInitializerError|java\.net\.BindException/i)) {
				var msg = "";

				if (data.match(/java\.net\.BindException/i)) {
					msg = "The server address is already in use. Please close any other apps/services that may be running on port " + serverPort + ".";
				} else if (data.match(/Database may be already in use/i)) {
					msg = "The server database is already in use. Please close any other apps/services that may be connected to this database.";
				} else {
					msg = "A server initialization error occurred.";
				}

				showInitializationAlert(msg, function() {
					systemClose = true;
					nxtInitializationError = false;
					if (nrs.exitCode == null) {
						nrs.kill();
					} else {
						win.close();
					}
				});
			}
		});

		nrs.on("exit", function(code) {
			if (callback) {
				callback();
				callback = null;
				return;
			} else if (nxtInitializationError) {
				return;
			} else if (!systemClose) {
				document.getElementById("loading_indicator_container").style.display = "none";
				showAlertAndQuit("NRS server has exited.");
			} else if (!doNotQuit) {
				win.close();
			}
		});
	} catch (err) {
		showInitializationAlert();
	}
}

function serverStarted() {
	isStarted = true;

	addUpdateBehavior();

	document.getElementById("nrs").setAttribute("src", "http://localhost:" + serverPort + "?app=win-2.2.0");
}

function checkServerOutput(data) {
	if (!nxtVersion) {
		var match = data.match(/server version ([0-9\.]+[a-zA-Z]?)/i);

		if (match && match[1]) {
			nxtVersion = match[1];
		}
	}

	if (!isStarted) {
		if (data.match(/nxt\.isTestnet = "true"/i)) {
			isTestNet = true;
			netSwitcher.label = "Switch to Main Net";
			serverPort = "6876";
		} else if (data.match(/nxt\.isTestnet = "false"/i)) {
			isTestNet = false;
			netSwitcher.label = "Switch to Test Net";
		}

		if (data.match(/started successfully/i)) {
			serverStarted();
		}
	}
}

function logServerOutput(data) {
	serverOutput.push(data);
	if (serverOutput.length > 100) {
		serverOutput.shift();
	}

	if (nxtInitializationError || systemClose) {
		return;
	}

	if (!isStarted) {
		var opacities = [0.6, 0.7, 0.8, 0.9, 1];

		data = data.split("\n");

		var lastLines = [];

		for (var i = data.length; i >= 0; i--) {
			if (data[i]) {
				data[i] = $.trim(data[i].replace(/^\s*[0-9\s\-:\.]+\s*(INFO\s*:\s*)?/i, ""));

				if (data[i] && !data[i].match(/(^nxt)|enabled|disabled|database is at level|Invalid well known peer|FINE:|:INFO:|genesis block|\.\.\.done|DEBUG/i)) {
					var opacity = opacities.pop();

					lastLines.push("<span style='opacity:" + opacity + "'>" + String(data[i]).escapeHTML() + "</span>");
					if (lastLines.length == 5) {
						break;
					}
				}
			}
		}

		if (lastLines.length) {
			lastLines.reverse();

			document.getElementById("server_output").innerHTML = lastLines.join("<br />");
		}
	}
}

function logMinterOutput(data) {
	minterOutput.push(data);
	if (minterOutput.length > 100) {
		minterOutput.shift();
	}
}

function viewServerLog() {
	if (showQuitAlert) {
		return;
	}

	hideAlerts();

	var log = serverOutput.join("\n");

	log = log.replace(/\n\s*\n/g, "\n");

	bootbox.dialog({
		message: "<p>Below are the last 100 messages from the server log:</p>" +
			"<textarea style='margin-top:10px;width:100%;' rows='6' class='form-control'>" + String(log).escapeHTML() + "</textarea>",
		title: "Server Log",
		buttons: {
			ok: {
				label: "OK",
				className: "btn-primary"
			}
		}
	});
}

function viewMinterLog() {
	if (showQuitAlert) {
		return;
	}

	hideAlerts();

	if (minterOutput && minterOutput.length) {
		var log = minterOutput.join("\n");

		log = log.replace(/\n\s*\n/g, "\n");

		bootbox.dialog({
			message: "<p>Below are the last 100 messages from the minter log:</p>" +
				"<textarea style='margin-top:10px;width:100%;' rows='6' class='form-control'>" + String(log).escapeHTML() + "</textarea>",
			title: "Minter Log",
			buttons: {
				ok: {
					label: "OK",
					className: "btn-primary"
				}
			}
		});
	} else {
		bootbox.alert("The minter log is currently empty.");
	}
}

function addUpdateBehavior() {
	if (preferences.checkForUpdates) {
		checkForUpdates();

		//once per day..
		updateInterval = setInterval(function() {
			checkForUpdates();
		}, 86400000);
	} else {
		clearInterval(updateInterval);
	}
}

function checkForUpdates() {
	if (!isStarted || nxtInitializationError || showQuitAlert) {
		manuallyCheckingForUpdates = false;
		return;
	}

	hideAlerts();

	if (isTestNet) {
		if (manuallyCheckingForUpdates) {
			showAlert("To check for updates you need to be connected to the main net, not the test net.");
			manuallyCheckingForUpdates = false;
		}
		return;
	}

	var versions = {};

	if (preferences.checkForUpdates == 2) {
		var normalRequest = http.get("http://localhost:" + serverPort + "/nxt?requestType=getAlias&aliasName=nrsversion", function(res) {
			var body = "";

			res.on("data", function(chunk) {
				body += chunk;
			}).on("end", function() {
				if (body.match(/errorCode/i)) {
					versions.normal = {
						"version": "0.0.0",
						"hash": ""
					};
				} else {
					versions.normal = body;
				}
				checkForNewestUpdate(versions);
			}).on("error", function() {
				versions.normal = "error";
				checkForNewestUpdate(version);
			});
		}).on("error", function() {
			versions.normal = "error";
			checkForNewestUpdate(versions);
		});

		normalRequest.end();

		var betaRequest = http.get("http://localhost:" + serverPort + "/nxt?requestType=getAlias&aliasName=nrsbetaversion", function(res) {
			var body = "";

			res.on("data", function(chunk) {
				body += chunk;
			}).on("end", function() {
				if (body.match(/errorCode/i)) {
					versions.beta = {
						"version": "0.0.0",
						"hash": ""
					};
				} else {
					versions.beta = body;
				}
				checkForNewestUpdate(versions);
			}).on("error", function() {
				versions.beta = "error";
				checkForNewestUpdate(versions);
			});
		}).on("error", function() {
			versions.beta = "error";
			checkForNewestUpdate(versions);
		});

		betaRequest.end();
	} else {
		var normalRequest = http.get("http://localhost:" + serverPort + "/nxt?requestType=getAlias&aliasName=nrsversion", function(res) {
			var body = "";

			res.on("data", function(chunk) {
				body += chunk;
			}).on("end", function() {
				if (body.match(/errorCode/i)) {
					checkForUpdatesCompleted({
						"version": "0.0.0",
						"hash": ""
					});
				} else {
					var version = parseVersionAlias(body);
					checkForUpdatesCompleted(version);
				}
			}).on("error", function() {
				checkForUpdatesFailed();
			});
		}).on("error", function() {
			checkForUpdatesFailed();
		});

		normalRequest.end();
	}
}

function downloadUpdateType(type) {
	if (type == "release") {
		var normalRequest = http.get("http://localhost:" + serverPort + "/nxt?requestType=getAlias&aliasName=nrsversion", function(res) {
			var body = "";

			res.on("data", function(chunk) {
				body += chunk;
			}).on("end", function() {
				if (body.match(/errorCode/i)) {
					checkForUpdatesCompleted({
						"version": "0.0.0",
						"hash": ""
					});
				} else {
					var version = parseVersionAlias(body);
					checkForUpdatesCompleted(version);
				}
			}).on("error", function() {
				checkForUpdatesFailed();
			});
		}).on("error", function() {
			checkForUpdatesFailed();
		});

		normalRequest.end();
	} else {
		var betaRequest = http.get("http://localhost:" + serverPort + "/nxt?requestType=getAlias&aliasName=nrsbetaversion", function(res) {
			var body = "";

			res.on("data", function(chunk) {
				body += chunk;
			}).on("end", function() {
				if (body.match(/errorCode/i)) {
					checkForUpdatesCompleted({
						"version": "0.0.0",
						"hash": ""
					});
				} else {
					var version = parseVersionAlias(body);
					checkForUpdatesCompleted(version);
				}
			}).on("error", function() {
				checkForUpdatesFailed();
			});
		}).on("error", function() {
			checkForUpdatesFailed();
		});

		betaRequest.end();
	}
}

function checkForNewestUpdate(versions) {
	if (!versions.beta || !versions.normal) {
		return;
	}

	if (versions.beta == "error" && versions.normal == "error") {
		checkForUpdatesFailed();
	} else if (versions.beta == "error") {
		checkForUpdatesCompleted(versions.normal);
	} else if (versions.normal == "error") {
		checkForUpdatesCompleted(versions.beta);
	} else {
		if (typeof versions.normal == "string") {
			var normal = parseVersionAlias(versions.normal);
		} else {
			var normal = versions.normal;
		}
		if (typeof versions.beta == "string") {
			var beta = parseVersionAlias(versions.beta);
		} else {
			var beta = versions.beta;
		}

		var result = versionCompare(normal.version, beta.version);

		if (result == 1) {
			checkForUpdatesCompleted(normal);
		} else {
			checkForUpdatesCompleted(beta);
		}
	}
}

function parseVersionAlias(contents) {
	if (!contents) {
		return {
			"version": "",
			"hash": ""
		};
	} else {
		contents = JSON.parse(contents);
		contents = contents.aliasURI.split(" ");

		return {
			"version": contents[0],
			"hash": contents[1]
		};
	}
}

function checkForUpdatesCompleted(update) {
	//this is done to prevent showing update notices whilst blockchain is still downloading.. 
	//should use another method later (messaging)
	if (versionCompare(update.version, "1.4.0") != 1) {
		if (manuallyCheckingForUpdates) {
			showAlert("Try again in a little bit, the blockchain is still downloading...");
			manuallyCheckingForUpdates = false;
		}
		return;
	}

	if (update.version) {
		var result = versionCompare(update.version, nxtVersion);

		if (result == 1) {
			if (manuallyCheckingForUpdates && !update.hash) {
				showAlert("The hash was not found, the update will not proceed.");
			} else {
				var changelogReq = https.get("https://bitbucket.org/JeanLucPicard/nxt/downloads/nxt-client-" + update.version + ".changelog.txt.asc", function(res) {
					var changelog = "";

					res.on("data", function(chunk) {
						changelog += chunk;
					}).on("end", function() {
						update.changelog = changelog;
						showUpdateNotice(update);
					}).on("error", function(e) {
						showUpdateNotice(update);
					});
				}).on("error", function(e) {
					showUpdateNotice(update);
				});

				changelogReq.end();
			}
		} else if (manuallyCheckingForUpdates) {
			showAlert("You are already using the latest version of the Nxt client (" + String(nxtVersion).escapeHTML() + ").");
		}
	} else if (manuallyCheckingForUpdates) {
		showAlert("Update information was not found, please try again later.");
	}

	manuallyCheckingForUpdates = false;
}

function checkForUpdatesFailed() {
	if (manuallyCheckingForUpdates) {
		showAlert("Could not connect to the update server, please try again later.");
		manuallyCheckingForUpdates = false;
	}
}

function showUpdateNotice(update) {
	if (showQuitAlert) {
		return;
	}

	document.getElementById("loading_indicator_container").style.display = "none";
	document.getElementById("server_output_container").style.display = "none";

	if (!update.changelog) {
		bootbox.confirm("A new version of the Nxt client is available (" + String(update.version).escapeHTML() + "). Would you like to update?", function(result) {
			if (result) {
				downloadNxt(update);
			}
		});
	} else {
		bootbox.dialog({
			message: "<p>A new version of the Nxt client is available (" + String(update.version).escapeHTML() + "). Would you like to update?</p>" +
				"<textarea style='margin-top:10px;width:100%;' rows='6' class='form-control'>" + String(update.changelog).escapeHTML() + "</textarea>",
			title: "Update Available",
			buttons: {
				cancel: {
					label: "Cancel",
					className: "btn-default"
				},
				update: {
					label: "OK",
					className: "btn-primary",
					callback: function() {
						downloadNxt(update);
					}
				}
			}
		});
	}
}

function downloadNxt(update) {
	if (showQuitAlert) {
		return;
	}

	bootbox.dialog({
		message: "<p>The new client is being downloaded. Upon completion the app will restart.</p>" +
			"<div class='progress progress-striped active' style='margin-top:10px'>" +
			"<div id='nrs_update_progress' class='progress-bar' role='progressbar' aria-valuemin='0' aria-valuemax='100' style='width:0%'>" +
			"<span class='sr-only'>0% Complete</span>" +
			"</div>" +
			"</div>",
		title: "Updating Nxt Client...",
		closeButton: false
	});

	var temp = require("temp");

	temp.track();

	temp.mkdir("nxt-client", function(err, dirPath) {
		if (err) {
			installNxtFailed();
			return;
		}

		var downloadReq = https.get("https://bitbucket.org/JeanLucPicard/nxt/downloads/nxt-client-" + update.version + ".zip", function(res) {
			var len = parseInt(res.headers["content-length"], 10);
			var cur = 0;
			var total = len / 1048576; //bytes in 1 MB

			var zipPath = path.join(dirPath, "nxt-client-" + update.version + ".zip");

			var out = fs.createWriteStream(zipPath);

			res.pipe(out);

			res.on("data", function(chunk) {
				cur += chunk.length;
				document.getElementById("nrs_update_progress").style.width = (100.0 * cur / len).toFixed(2) + "%";
			}).on("end", function() {
				installNxt(zipPath, update.hash);
			}).on("error", function(e) {
				installNxtFailed();
			});
		}).on("error", function(e) {
			installNxtFailed();
		});

		downloadReq.end();
	});
}

function installNxt(zipPath, correctHash) {
	var AdmZip = require("adm-zip");
	var crypto = require("crypto");
	var algo = "sha256";

	var hash = crypto.createHash(algo);

	var zipData = fs.ReadStream(zipPath);

	zipData.on("data", function(d) {
		hash.update(d);
	}).on("end", function() {
		var d = hash.digest("hex");

		if (d !== correctHash) {
			showAlert("The hash of the downloaded update does not equal the one supplied by the blockchain. Aborting update.");
		} else {
			killNrs(function() {
				try {
					var zip = new AdmZip(zipPath);
					zip.extractAllTo(path.normalize(nxtDirectory + "../"), /*overwrite*/ true);
				} catch (e) {}
				relaunchApplication();
			});
		}
	}).on("error", function() {
		installNxtFailed();
	});
}

function showKillAlert() {
	hideAlerts();

	bootbox.dialog({
		message: "Exiting NRS... Please wait.",
		title: "Shutdown In Progress",
		buttons: {}
	});
}

function killNrs(fn) {
	showKillAlert();

	setTimeout(function() {
		if (nrs && nrs.exitCode == null) {
			killAlreadyRunningProcess(null, true);
			callback = fn;
			nrs.kill();
		} else {
			killAlreadyRunningProcess(null, true);
			callback = null;
			fn();
		}
	}, 500);
}

function installNxtFailed() {
	showAlert("Could not connect to the update server, please try again later.");
}

function bodyLoaded() {
	setTimeout(function() {
		win.show();
	}, 150);

	findNxtDirectory(function() {
		//generateConfig();
		startServer();
	});
}

function switchNet() {
	systemClose = true;
	doNotQuit = true;

	killNrs(function() {
		addToConfig({
			"nxt.isTestnet": isTestNet ? "false" : "true"
		});

		relaunchApplication();
	});
}

function redownloadBlockchain() {
	systemClose = true;
	doNotQuit = true;

	killNrs(function() {
		var appData = gui.App.dataPath;

		if (isTestNet) {
			var filesToRemove = ["nxt_test_db" + dirSeparator + "nxt.h2.db", "nxt_test_db" + dirSeparator + "nxt.lock.db", "nxt_test_db" + dirSeparator + "nxt.trace.db"];
		} else {
			var filesToRemove = ["nxt_db" + dirSeparator + "nxt.h2.db", "nxt_db" + dirSeparator + "nxt.lock.db", "nxt_db" + dirSeparator + "nxt.trace.db"];
		}

		try {
			for (var i = 0; i < filesToRemove.length; i++) {
				fs.unlinkSync(appData + dirSeparator + "nxt" + dirSeparator + filesToRemove[i]);
			}
		} catch (err) {}

		relaunchApplication();
	});
}

function escapeRegExp(str) {
	return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function generateConfig() {
	/*
	var appData = gui.App.dataPath;

	var settings = {
		"nxt.dbUrl": "jdbc:h2:" + appData.replace(new RegExp(escapeRegExp(dirSeparator), "g"), "/") + "/nxt_db/nxt;DB_CLOSE_ON_EXIT=FALSE",
		"nxt.testDbUrl": "jdbc:h2:" + appData.replace(new RegExp(escapeRegExp(dirSeparator), "g"), "/") + "/nxt_test_db/nxt;DB_CLOSE_ON_EXIT=FALSE"
	};

	addToConfig(settings);*/
}

function getConfigFilePath() {
	var configFile = nxtDirectory + "conf" + dirSeparator + "nxt.properties";

	try {
		if (!fs.existsSync(configFile)) {
			fs.writeFile(configFile, "");
		}
	} catch (err) {

	}

	return configFile;
}

function getConfigFile() {
	return fs.readFileSync(getConfigFilePath(), "utf8");
}

function getConfig(keys) {
	var config = {};

	var contents = getConfigFile();

	var lines = contents.match(/[^\r\n]+/g);

	for (var i = 0; i < lines.length; i++) {
		var setting = lines[i].explode("=", 2);

		if (setting.length == 2) {
			var settingKey = String(setting[0]).trim();
			var settingValue = String(setting[1]).trim();

			if (!keys || keys.indexOf(settingKey) != -1) {
				config[settingKey] = settingValue;
			}
		}
	}

	return config;
}

function addToConfig(settings) {
	var config = getConfig();

	for (var settingKey in settings) {
		config[settingKey] = settings[settingKey];
	}

	var newContents = "";

	for (var settingKey in config) {
		newContents += settingKey + "=" + config[settingKey] + "\r\n";
	}

	if (newContents != getConfigFile()) {
		try {
			fs.writeFileSync(getConfigFilePath(), newContents, {
				"encoding": "utf8"
			});
		} catch (err) {
			alert("Could not write to config file.");
		}
	}
}

function versionCompare(v1, v2) {
	if (v2 == undefined) {
		return -1;
	} else if (v1 == undefined) {
		return -1;
	}

	//https://gist.github.com/TheDistantSea/8021359 (based on)
	var v1last = v1.slice(-1);
	var v2last = v2.slice(-1);

	if (v1last == 'e') {
		v1 = v1.substring(0, v1.length - 1);
	} else {
		v1last = '';
	}

	if (v2last == 'e') {
		v2 = v2.substring(0, v2.length - 1);
	} else {
		v2last = '';
	}

	var v1parts = v1.split('.');
	var v2parts = v2.split('.');

	function isValidPart(x) {
		return /^\d+$/.test(x);
	}

	if (!v1parts.every(isValidPart) || !v2parts.every(isValidPart)) {
		return NaN;
	}

	v1parts = v1parts.map(Number);
	v2parts = v2parts.map(Number);

	for (var i = 0; i < v1parts.length; ++i) {
		if (v2parts.length == i) {
			return 1;
		}
		if (v1parts[i] == v2parts[i]) {
			continue;
		} else if (v1parts[i] > v2parts[i]) {
			return 1;
		} else {
			return -1;
		}
	}

	if (v1parts.length != v2parts.length) {
		return -1;
	}

	if (v1last && v2last) {
		return 0;
	} else if (v1last) {
		return 1;
	} else if (v2last) {
		return -1;
	} else {
		return 0;
	}
}

function hideAlerts() {
	bootbox.hideAll();
}

function showAlert(msg) {
	if (showQuitAlert) {
		return;
	}

	hideAlerts();

	bootbox.alert(msg);
}

function showAlertAndQuit(msg, callback) {
	if (showQuitAlert) {
		return;
	}

	showQuitAlert = true;

	hideAlerts();

	if (!msg) {
		msg = "An error occurred, the server has quit. Please restart the application.";
	}

	bootbox.alert(msg, function() {
		if (callback) {
			callback();
		} else {
			win.close();
		}
	});
}

function showInitializationAlert(msg, callback) {
	if (nxtInitializationError) {
		return;
	}

	nxtInitializationError = true;

	document.getElementById("loading_indicator_container").style.display = "none";
	document.getElementById("server_output").innerHTML = "Exception occurred";

	showAlertAndQuit(msg, callback);
}

function getAlreadyRunningProcess(command) {
	if (!command) {
		return 0;
	}

	try {
		var output = execSync("wmic process where commandline='" + String(command).replace(/\\/g, "\\\\") + "' get processid");
		process.stdout.write(output);

		output = output.toString();

		var lines = output.match(/[^\r\n]+/g);

		if (lines.length >= 2) {
			var pid = String(lines[1]).trim();
			if (pid.match(/^[0-9]+$/)) {
				return pid;
			}
		}
	} catch (err) {}

	return 0;
}

window.addEventListener("message", receiveMessage, false);

function receiveMessage(event) {
	if (event.origin != "http://localhost:7876" && event.origin != "http://localhost:6876") {
		return;
	}

	if (typeof event.data == "object") {
		if (event.data.type == "copy") {
			var clipboard = gui.Clipboard.get();
			clipboard.set(event.data.text, "text");
		} else if (event.data.type == "update") {
			manuallyCheckingForUpdates = true;
			downloadUpdateType(event.data.update.type);
		} else if (event.data.type == "language") {

		} else if (event.data.type == "appUpdate") {

		}
	} else if (event.data == "loaded") {
		document.getElementById("nrs_container").style.display = "block";
		document.getElementById("loading_indicator_container").style.display = "none";
		document.getElementById("server_output_container").style.display = "none";

		try {
			document.getElementById("nrs").contentWindow.document.body.addEventListener("contextmenu", showContextMenu, false);
		} catch (e) {}
	}
}